<?php

/**
 * Build the data.json from the API.
 */

require 'vendor/autoload.php';

define('API_URL', 'https://apis.digital.gob.cl/dpa');

$data = [];
$client = new GuzzleHttp\Client();
$response_regions = $client->request('GET', API_URL . '/regiones');

$regions = json_decode($response_regions->getBody());

foreach ($regions as $region) {
    $region_id = $region->codigo;
    $data[$region_id] = [
        'id' => $region_id,
        'type' => 'region',
        'name' => $region->nombre,
        'lat' => $region->lat,
        'lng' => $region->lng,
        'url' => $region->url,
        'parent' => $region->codigo_padre,
        'provinces' => [],
        'communes' => [],
    ];

    $url = API_URL . "/regiones/{$region_id}/provincias";
    echo "Search provincias: $url \n";
    $response_provinces = $client->request('GET', $url);
    $provinces = json_decode($response_provinces->getBody());

    foreach ($provinces as $province) {
        $province_id = $province->codigo;
        $data[$region_id]['provinces'][$province_id] = [
            'id' => $province_id,
            'type' => 'province',
            'name' => $province->nombre,
            'lat' => $province->lat,
            'lng' => $province->lng,
            'url' => $province->url,
            'parent' => $province->codigo_padre,
            'communes' => [],
        ];

        $response_communes = $client->request('GET', API_URL . "/provincias/{$province_id}/comunas");
        $communes = json_decode($response_communes->getBody());

        foreach ($communes as $commune) {
            $commune_id = $commune->codigo;
            $commune_data = [
                'id' => $commune_id,
                'type' => 'commune',
                'name' => $commune->nombre,
                'lat' => $commune->lat,
                'lng' => $commune->lng,
                'url' => $commune->url,
                'parent' => $commune->codigo_padre,
            ];
            $data[$region_id]['provinces'][$province_id]['communes'][$commune_id] = $commune_data;
            $data[$region_id]['communes'][$commune_id] = $commune_data;
        }
    }
}

file_put_contents('data.json', json_encode($data, JSON_PRETTY_PRINT));